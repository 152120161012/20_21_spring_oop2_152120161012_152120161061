﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;



namespace GroceryStoreApp
{
    
    public partial class Form1 : Form
    {
        // Before you build the project, images folder must be in the same place with the solution.
        string wanted_path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
        public double totalprice = 0;

        public Form1()
        {
            InitializeComponent();
        }
        public class Accounts
        {
            private string userName = "acc";
            private string password = "acc";
            private string userName2 = "acc2";
            private string password2 = "acc2";
            private string userName3 = "admin";
            private string password3 = "admin";
            private string accountName = "Mike";
            private string accountName2 = "Sally";
            private string accountName3 = "Admin";

            public Accounts()
            {
                
            }

            public string Login(string userName, string password)
            {
                if (userName == this.userName && password == this.password) return accountName;
                if (userName == this.userName2 && password == this.password2) return accountName2;
                if (userName == this.userName3 && password == this.password3) return accountName3;
                else return null;
            }


        }

        private void Products_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listbox1.SelectedItem == "Apple")
            {
                string fileName = "Apple.png";
                string path = Path.Combine(wanted_path,@"images\", fileName);
                pictureBox1.Image = Image.FromFile(path);
                label3.Text = Apple.Price;
                label4.Text = Apple.Name;
                label5.Text = Apple.Description;
            }
            if (listbox1.SelectedItem == "Milk")
            {
                string fileName = "Milk.png";             
                string path = Path.Combine(wanted_path, @"images\", fileName);
                pictureBox1.Image = Image.FromFile(path);
                label3.Text = Milk.Price;
                label4.Text = Milk.Name;
                label5.Text = Milk.Description;
            }
            if (listbox1.SelectedItem == "Steak")
            {
                string fileName = "Steak.jpg";
                string path = Path.Combine(wanted_path, @"images\", fileName);
                pictureBox1.Image = Image.FromFile(path);
                label3.Text = Steak.Price;
                label4.Text = Steak.Name;
                label5.Text = Steak.Description;
            }
            if (listbox1.SelectedItem == "Egg")
            {
                string fileName = "Egg.jpg";
                string path = Path.Combine(wanted_path, @"images\", fileName);
                pictureBox1.Image = Image.FromFile(path);
                label3.Text = Egg.Price;
                label4.Text = Egg.Name;
                label5.Text = Egg.Description;
            }
            if (listbox1.SelectedItem == "Coffee")
            {
                string fileName = "Coffee.jpg";
                string path = Path.Combine(wanted_path, @"images\", fileName);
                pictureBox1.Image = Image.FromFile(path);
                label3.Text = Coffee.Price;
                label4.Text = Coffee.Name;
                label5.Text = Coffee.Description;
            }
            if (listbox1.SelectedItem == "Beer")
            {
                string fileName = "Beer.jpg";
                string path = Path.Combine(wanted_path, @"images\", fileName);
                pictureBox1.Image = Image.FromFile(path);
                label3.Text = Beer.Price;
                label4.Text = Beer.Name;
                label5.Text = Beer.Description;
            }
            if (listbox1.SelectedItem == "Chips")
            {
                string fileName = "Chips.jpg";
                string path = Path.Combine(wanted_path, @"images\", fileName);
                pictureBox1.Image = Image.FromFile(path);
                label3.Text = Chips.Price;
                label4.Text = Chips.Name;
                label5.Text = Chips.Description;
            }
            if (listbox1.SelectedItem == "Salmon")
            {
                string fileName = "Salmon.jpg";
                string path = Path.Combine(wanted_path, @"images\", fileName);
                pictureBox1.Image = Image.FromFile(path);
                label3.Text = Salmon.Price;
                label4.Text = Salmon.Name;
                label5.Text = Salmon.Description;
            }
            if (listbox1.SelectedItem == "Yogurt")
            {
                string fileName = "Yogurt.jpg";
                string path = Path.Combine(wanted_path, @"images\", fileName);
                pictureBox1.Image = Image.FromFile(path);
                label3.Text = Yogurt.Price;
                label4.Text = Yogurt.Name;
                label5.Text = Yogurt.Description;
            }
            if (listbox1.SelectedItem == "Cheese")
            {
                string fileName = "Cheese.jpg";
                string path = Path.Combine(wanted_path, @"images\", fileName);
                pictureBox1.Image = Image.FromFile(path);
                label3.Text = Cheese.Price;
                label4.Text = Cheese.Name;
                label5.Text = Cheese.Description;
            }
        }
    
        private void button1_Click(object sender, EventArgs e)
        {
            switch (listbox1.SelectedItem)
            {
                case "Apple":
                    {
                        totalprice += Convert.ToDouble(Apple.Price);
                        label9.Text = Apple.Description;
                    }break;
                case "Milk":
                    {
                        totalprice += Convert.ToDouble(Milk.Price);
                        label10.Text = Milk.Description;
                    }
                    break;
                case "Steak":
                    {
                        totalprice += Convert.ToDouble(Steak.Price);
                        label11.Text = Steak.Description;
                    }
                    break;
                case "Egg":
                    {
                        totalprice += Convert.ToDouble(Egg.Price);
                        label12.Text = Egg.Description;
                    }
                    break;
                case "Coffee":
                    {
                        totalprice += Convert.ToDouble(Coffee.Price);
                        label13.Text = Coffee.Description;
                    }
                    break;
                case "Beer":
                    {
                        totalprice += Convert.ToDouble(Beer.Price);
                        label14.Text = Beer.Description;
                    }
                    break;
                case "Chips":
                    {
                        totalprice += Convert.ToDouble(Chips.Price);
                        label15.Text = Chips.Description;
                    }
                    break;
                case "Salmon":
                    {
                        totalprice += Convert.ToDouble(Salmon.Price);
                        label16.Text = Salmon.Description;
                    }
                    break;
                case "Yogurt":
                    {
                        totalprice += Convert.ToDouble(Yogurt.Price);
                        label17.Text = Yogurt.Description;
                    }
                    break;
                case "Cheese":
                    {
                        totalprice += Convert.ToDouble(Cheese.Price);
                        label18.Text = Cheese.Description;
                    }
                    break;
            }
        }
        

        private void button2_Click(object sender, EventArgs e)
        {
            switch (listbox1.SelectedItem)
            {
                case "Apple":
                    {
                        totalprice -= Convert.ToDouble(Apple.Price);
                        label9.Text = null;
                    }
                    break;
                case "Milk":
                    {
                        totalprice -= Convert.ToDouble(Milk.Price);
                        label10.Text = null;
                    }
                    break;
                case "Steak":
                    {
                        totalprice -= Convert.ToDouble(Steak.Price);
                        label11.Text = null;
                    }
                    break;
                case "Egg":
                    {
                        totalprice -= Convert.ToDouble(Egg.Price);
                        label12.Text = null;
                    }
                    break;
                case "Coffee":
                    {
                        totalprice -= Convert.ToDouble(Coffee.Price);
                        label13.Text = null;
                    }
                    break;
                case "Beer":
                    {
                        totalprice -= Convert.ToDouble(Beer.Price);
                        label14.Text = null;
                    }
                    break;
                case "Chips":
                    {
                        totalprice -= Convert.ToDouble(Chips.Price);
                        label15.Text = null;
                    }
                    break;
                case "Salmon":
                    {
                        totalprice -= Convert.ToDouble(Salmon.Price);
                        label16.Text = null;
                    }
                    break;
                case "Yogurt":
                    {
                        totalprice -= Convert.ToDouble(Yogurt.Price);
                        label17.Text = null;
                    }
                    break;
                case "Cheese":
                    {
                        totalprice -= Convert.ToDouble(Cheese.Price);
                        label18.Text = null;
                    }
                    break;
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            label7.Text = totalprice.ToString();
        }
        public class Steak
        {
            public static string Price = "35";
            public static string Name = "Steak";
            public static string Description = "1kg American Wagyu Roastbeef Steak.";
        }
        public class Apple
        {
            public static string Price = "1.25";
            public static string Name = "Apple";
            public static string Description = "1kg Apple.";
        }

        public class Egg
        {
            public static string Price = "0.99";
            public static string Name = "Egg";
            public static string Description = "12x eggs.";
        }
        public class Milk
        {
            public static string Price = "1.99";
            public static string Name = "Milk";
            public static string Description = "5kg milk.";
        }
        public class Coffee
        {
            public static string Price = "4.99";
            public static string Name = "Coffee";
            public static string Description = "Black Coffee.";
        }
        public class Beer
        {
            public static string Price = "3.99";
            public static string Name = "Beer";
            public static string Description = "6x Malt Beer.";
        }
        public class Chips
        {
            public static string Price = "1.50";
            public static string Name = "Chipps";
            public static string Description = "Potatoe chips.";
        }
        public class Salmon
        {
            public static string Price = "9.99";
            public static string Name = "Salmon";
            public static string Description = "1kg Salmon.";
        }
        public class Yogurt
        {
            public static string Price = "0.75";
            public static string Name = "Yogurt";
            public static string Description = "1kg Yogurt.";
        }
        public class Cheese
        {
            public static string Price = "2.25";
            public static string Name = "Cheese";
            public static string Description = "1kg Parmesan Cheese.";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Accounts log = new Accounts();
             label21.Text = log.Login(textBox1.Text, textBox2.Text);
        }

        
    }
}
