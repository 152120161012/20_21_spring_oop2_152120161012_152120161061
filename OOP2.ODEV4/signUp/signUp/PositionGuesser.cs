﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace signUp
{
    public partial class PositionGuesser : Form
    {
        public PositionGuesser()
        {
            InitializeComponent();
        }

        private void PositionGuesser_Load(object sender, EventArgs e)
        {

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            label1.Text = "512";
            label2.Text = "512";
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            label1.Text = "0";
            label2.Text = "0";
        }

        private void btnNorth_Click(object sender, EventArgs e)
        {
            label2.Text = BinarySearch.binarySearchYNorth(Convert.ToInt32(label2.Text)).ToString();
        }

        private void btnSouth_Click(object sender, EventArgs e)
        {
            label2.Text = BinarySearch.binarySearchYSouth(Convert.ToInt32(label2.Text)).ToString();
        }

        private void btnEast_Click(object sender, EventArgs e)
        {
            label1.Text = BinarySearch.binarySearchXEast(Convert.ToInt32(label1.Text)).ToString();
        }

        private void btnWest_Click(object sender, EventArgs e)
        {
            label1.Text = BinarySearch.binarySearchXWest(Convert.ToInt32(label1.Text)).ToString();
        }

        private void btnNorthEast_Click(object sender, EventArgs e)
        {
            label2.Text = BinarySearch.binarySearchYNorth(Convert.ToInt32(label2.Text)).ToString();
            label1.Text = BinarySearch.binarySearchXEast(Convert.ToInt32(label1.Text)).ToString();
        }

        private void btnNorthWest_Click(object sender, EventArgs e)
        {
            label2.Text = BinarySearch.binarySearchYNorth(Convert.ToInt32(label2.Text)).ToString();
            label1.Text = BinarySearch.binarySearchXWest(Convert.ToInt32(label1.Text)).ToString();
        }

        private void btnSouthEast_Click(object sender, EventArgs e)
        {
            label2.Text = BinarySearch.binarySearchYSouth(Convert.ToInt32(label2.Text)).ToString();
            label1.Text = BinarySearch.binarySearchXEast(Convert.ToInt32(label1.Text)).ToString();
        }

        private void btnSouthWest_Click(object sender, EventArgs e)
        {
            label2.Text = BinarySearch.binarySearchYSouth(Convert.ToInt32(label2.Text)).ToString();
            label1.Text = BinarySearch.binarySearchXWest(Convert.ToInt32(label1.Text)).ToString();
        }
    }
}
