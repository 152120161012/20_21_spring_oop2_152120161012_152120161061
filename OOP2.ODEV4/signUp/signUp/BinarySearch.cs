﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace signUp
{
    class BinarySearch
    {
        public static int binarySearchXEast(int x)
        {
            int left = x;

            int right = 1024;

            return (right + left) / 2;
        }
        public static int binarySearchXWest(int x)
        {
            int left = 0;

            int right = x;

            return (right + left) / 2;
        }
        public static int binarySearchYNorth(int y)
        {
            int left = y;

            int right = 1024;

            return (right + left) / 2;
        }
        public static int binarySearchYSouth(int y)
        {
            int left = 0;

            int right = y;

            return (right + left) / 2;
        }
    }
}
